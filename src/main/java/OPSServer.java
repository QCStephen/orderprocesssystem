import Service.OrderService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;
import java.util.ResourceBundle;

public class OPSServer {
    private static Server server =null;
    private static final int port = 9090;
    public static void main(String[] args) throws IOException, InterruptedException {
        server = ServerBuilder.forPort(port).addService(new OrderService()).build().start();
        System.out.println(String.format("OrderProcessSystem服务端已启动，端口号:%d", port));

        server.awaitTermination();
    }
}
