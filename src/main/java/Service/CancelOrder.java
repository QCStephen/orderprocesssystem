package Service;

import io.grpc.stub.StreamObserver;
import proto.ExecutionReport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CancelOrder {
    public synchronized void tryCancelOrder(long orderID, StreamObserver<ExecutionReport> responseObserver){
        ExecutionReport response = null;
        Connection conn = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;
        ResultSet rs = null;
        List<PreparedStatement> psList = new ArrayList<PreparedStatement>(2);
        try {
            conn = JDBCUtils.getConnection();
            conn.setAutoCommit(false);

            // 查询数据库
            pstmt1 = conn.prepareStatement("SELECT * FROM trade_order WHERE orderID=? For UPDATE");
            pstmt1.setLong(1, orderID);
            rs = pstmt1.executeQuery();
            if(rs.next()){
                pstmt2 = conn.prepareStatement("DELETE FROM trade_order WHERE orderID=?");
                pstmt2.setLong(1, orderID);
                int count = pstmt2.executeUpdate();
                response = ExecutionReport.newBuilder().setStat(ExecutionReport.STAT.CANCELED)
                        .setOrderID(orderID)
                        .setLeaveQty(0)
                        .build();
            }
            else{
                response = ExecutionReport.newBuilder().setStat(ExecutionReport.STAT.CANCEL_REJECT)
                        .setOrderID(orderID)
                        .setErrorMessage("您所取消的订单ID不存在")
                        .build();
            }
            conn.commit();
            psList.add(pstmt1);
            psList.add(pstmt2);
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        } catch (Exception e) {
            JDBCUtils.rollback(conn);
            response = ExecutionReport.newBuilder().setStat(ExecutionReport.STAT.CANCEL_REJECT)
                    .setOrderID(orderID)
                    .setErrorMessage("服务器发生异常")
                    .build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }finally {
            JDBCUtils.close(conn, psList, rs);
        }
    }
}
