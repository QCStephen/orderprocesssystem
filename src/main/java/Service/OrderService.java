package Service;

import io.grpc.stub.StreamObserver;
import proto.CancelOrderRequest;
import proto.ExecutionReport;
import proto.NewOrderRequest;
import proto.OrderServiceGrpc;

import java.util.HashMap;
import java.util.Map;


public class OrderService extends OrderServiceGrpc.OrderServiceImplBase {

    private NewOrder newOrder = new NewOrder();
    private CancelOrder cancelOrder = new CancelOrder();
    private Map<Long, StreamObserver<ExecutionReport>> responseMap = new HashMap<Long, StreamObserver<ExecutionReport>>();

    @Override
    public synchronized StreamObserver<NewOrderRequest> pushNewOrder(StreamObserver<ExecutionReport> responseObserver) {
        return new StreamObserver<NewOrderRequest>() {
            @Override
            public void onNext(NewOrderRequest newOrderRequest) {
                newOrder.tryNewOrder(newOrderRequest, responseObserver, responseMap);
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }

    @Override
    public synchronized void pushCancelOrder(CancelOrderRequest request, StreamObserver<ExecutionReport> responseObserver) {
        long orderID = request.getOrderID();
        System.out.println(String.format("收到对订单%s的撤单请求", orderID));
        try {
            cancelOrder.tryCancelOrder(orderID, responseObserver);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            System.out.println("服务端完成");
        }
    }
}
