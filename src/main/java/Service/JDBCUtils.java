package Service;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class JDBCUtils {

    private JDBCUtils(){}
    private static  DataSource ds;
    private static InputStream is;
    private static Properties pp;

    static {
//        try {
//            Class.forName("com.mysql.cj.jdbc.Driver");
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }


        is = JDBCUtils.class.getClassLoader().getResourceAsStream("druid.properties");

		pp = new Properties();
        try {
            pp.load(is);
            // 创建连接池，使用配置文件中的参数
            ds = DruidDataSourceFactory.createDataSource(pp);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }


//    public static Connection getConnection() throws SQLException {
//        String url = "jdbc:mysql://localhost:3306/mysql";
//        String user = "root";
//        String password = "123456";
//        return DriverManager.getConnection(url, user, password);
//    }

    public static void rollback(Connection conn){
        if(conn!=null){
            try {
                conn.rollback();
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        }
    }

    public static void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {}
        }

        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException e) {}
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {}
        }
    }

    public static void close(Connection conn, List<PreparedStatement> pstmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {}
        }

        for(PreparedStatement ps: pstmt){
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {}
            }
        }

        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {}
        }
    }
}
