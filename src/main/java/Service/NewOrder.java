package Service;

import io.grpc.stub.StreamObserver;
import proto.ExecutionReport;
import proto.NewOrderRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NewOrder {
    long i = 10000;
    public synchronized void tryNewOrder(NewOrderRequest newOrderRequest,
                                         StreamObserver<ExecutionReport> responseObserver,
                                         Map<Long, StreamObserver<ExecutionReport>> responseMap){
        Connection conn = null;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;
        PreparedStatement pstmt3 = null;
        PreparedStatement pstmt4 = null;
        PreparedStatement pstmt5 = null;
        PreparedStatement pstmt6 = null;
        PreparedStatement pstmt7 = null;
        List<PreparedStatement> psList = new ArrayList<PreparedStatement>(7);
        ResultSet rs = null;

        java.sql.Timestamp createTime = java.sql.Timestamp.valueOf(newOrderRequest.getTime());
        System.out.println(String.format("收到客户%s的报单请求", newOrderRequest.getClientID()));
        if (newOrderRequest.getOrderQty() <= 0 || newOrderRequest.getPrice() <= 0){
            responseObserver.onNext(ExecutionReport.newBuilder()
                    .setStat(ExecutionReport.STAT.ORDER_REJECT)
                    .setClientID(newOrderRequest.getClientID())
                    .setErrorMessage("订单请求中的交易数量或交易价格不合法")
                    .build());
        }
        else{
            try{
                conn = JDBCUtils.getConnection();
                conn.setAutoCommit(false);
                pstmt1=conn.prepareStatement("INSERT INTO trade_order (clientID,orderID,stockID,orderQty,orderPrice,fillQty,leaveQty,direction,createTime) VALUES (?,?,?,?,?,?,?,?,?)");
                pstmt1.setLong(1, newOrderRequest.getClientID());
                pstmt1.setLong(2, i);
                pstmt1.setString(3, newOrderRequest.getStockID());
                pstmt1.setInt(4, newOrderRequest.getOrderQty());
                pstmt1.setDouble(5, newOrderRequest.getPrice());
                pstmt1.setInt(6, 0);
                pstmt1.setInt(7, newOrderRequest.getOrderQty());
                pstmt1.setInt(8, newOrderRequest.getDirectionValue());
                pstmt1.setTimestamp(9, createTime);
                int count = pstmt1.executeUpdate();

                responseObserver.onNext(ExecutionReport.newBuilder()
                        .setStat(ExecutionReport.STAT.ORDER_ACCEPT)
                        .setClientID(newOrderRequest.getClientID())
                        .setOrderID(i)
                        .setStockID(newOrderRequest.getStockID())
                        .setOrderQty(newOrderRequest.getOrderQty())
                        .setOrderPrice(newOrderRequest.getPrice())
                        .build());

                responseMap.put(i, responseObserver);

                int direction = newOrderRequest.getDirectionValue();
                int oppositeDirection = direction == 0 ? 1 : 0;
                pstmt2 = oppositeDirection==0?
                        conn.prepareStatement("SELECT * FROM trade_order WHERE direction=? AND stockID=? AND orderPrice<=? ORDER BY orderPrice ASC, createTime ASC FOR UPDATE") :
                        conn.prepareStatement("SELECT * FROM trade_order WHERE direction=? AND stockID=? AND orderPrice>=? ORDER BY orderPrice DESC, createTime ASC FOR UPDATE");
                pstmt2.setInt(1, oppositeDirection);
                pstmt2.setString(2, newOrderRequest.getStockID());
                pstmt2.setDouble(3, newOrderRequest.getPrice());
                rs = pstmt2.executeQuery();
                int neworderQty = newOrderRequest.getOrderQty();

                while (rs.next()) {
                    // 新订单数量小于旧订单
                    if (neworderQty < rs.getInt(8)) {
                        // 更新旧订单
                        pstmt3 = conn.prepareStatement("UPDATE trade_order SET fillQty=?, leaveQty=? WHERE orderID=?");
                        pstmt3.setInt(1, neworderQty + rs.getInt(6));
                        pstmt3.setInt(2, rs.getInt(8) - neworderQty);
                        pstmt3.setLong(3, rs.getLong(2));
                        count = pstmt3.executeUpdate();

                        // 删除新订单
                        pstmt4 = conn.prepareStatement("DELETE FROM trade_order WHERE orderID=?");
                        pstmt4.setLong(1, i);
                        count = pstmt4.executeUpdate();

                        neworderQty = 0;

                        // 返回新订单响应
                        responseObserver.onNext(ExecutionReport.newBuilder()
                                .setStat(ExecutionReport.STAT.FILL)
                                .setClientID(newOrderRequest.getClientID())
                                .setOrderID(i)
                                .setStockID(newOrderRequest.getStockID())
                                .setOrderQty(newOrderRequest.getOrderQty())
                                .setOrderPrice(newOrderRequest.getPrice())
                                .setFillQty(neworderQty)
                                .setFillPrice(rs.getDouble(5))
                                .setLeaveQty(0)
                                .build());

                        // 返回旧报单响应
                        StreamObserver<ExecutionReport> old_responseObserver = responseMap.get(rs.getLong(2));
                        old_responseObserver.onNext(ExecutionReport.newBuilder()
                                .setStat(ExecutionReport.STAT.FILL)
                                .setClientID(rs.getLong(1))
                                .setOrderID(rs.getLong(2))
                                .setStockID(rs.getString(3))
                                .setOrderQty(rs.getInt(4))
                                .setOrderPrice(rs.getDouble(5))
                                .setFillQty(neworderQty)
                                .setFillPrice(rs.getDouble(5))
                                .setLeaveQty(rs.getInt(8) - neworderQty)
                                .build());
                        break;
                    }
                    // 新订单数量大于等于旧订单
                    else {
                        // 更新新订单
                        pstmt5 = conn.prepareStatement("UPDATE trade_order SET fillQty=?, leaveQty=? WHERE orderID=?");
                        pstmt5.setInt(1, newOrderRequest.getOrderQty() - neworderQty + rs.getInt(8));
                        pstmt5.setInt(2, neworderQty - rs.getInt(8));
                        pstmt5.setLong(3, i);
                        count = pstmt5.executeUpdate();

                        // 删除旧订单
                        pstmt6 = conn.prepareStatement("DELETE FROM trade_order WHERE orderID=?");
                        pstmt6.setLong(1, rs.getLong(2));
                        count = pstmt6.executeUpdate();

                        neworderQty -= rs.getInt(8);

                        // 新报单
                        responseObserver.onNext(ExecutionReport.newBuilder()
                                .setStat(ExecutionReport.STAT.FILL)
                                .setClientID(newOrderRequest.getClientID())
                                .setOrderID(i)
                                .setStockID(newOrderRequest.getStockID())
                                .setOrderQty(newOrderRequest.getOrderQty())
                                .setOrderPrice(newOrderRequest.getPrice())
                                .setFillQty(rs.getInt(8))
                                .setFillPrice(rs.getDouble(5))
                                .setLeaveQty(neworderQty - rs.getInt(8))
                                .build());

                        // 旧报单
                        StreamObserver<ExecutionReport> old_responseObserver = responseMap.get(rs.getLong(2));
                        responseObserver.onNext(ExecutionReport.newBuilder()
                                .setStat(ExecutionReport.STAT.FILL)
                                .setClientID(rs.getLong(1))
                                .setOrderID(rs.getLong(2))
                                .setStockID(rs.getString(3))
                                .setOrderQty(rs.getInt(4))
                                .setOrderPrice(rs.getDouble(5))
                                .setFillQty(rs.getInt(8))
                                .setFillPrice(rs.getDouble(5))
                                .setLeaveQty(0)
                                .build());
                        responseMap.remove(rs.getLong(2));
                    }
                }
                if (neworderQty == 0) {
                    pstmt7 = conn.prepareStatement("DELETE FROM trade_order WHERE orderID=?");
                    pstmt7.setLong(1, i);
                    count = pstmt7.executeUpdate();
                }
                conn.commit();
                psList.add(pstmt1);
                psList.add(pstmt2);
                psList.add(pstmt3);
                psList.add(pstmt4);
                psList.add(pstmt5);
                psList.add(pstmt6);
                psList.add(pstmt7);
                i += 1;
            } catch (Exception e) {
                e.printStackTrace();
                JDBCUtils.rollback(conn);
                responseObserver.onNext(ExecutionReport.newBuilder()
                        .setStat(ExecutionReport.STAT.CANCELED)
                        .setClientID(newOrderRequest.getClientID())
                        .setOrderID(i)
                        .setErrorMessage("服务器发生错误，交易失败，您的报单已取消")
                        .build());
            } finally {
                JDBCUtils.close(conn, psList, rs);
            }
        }
    }
}
