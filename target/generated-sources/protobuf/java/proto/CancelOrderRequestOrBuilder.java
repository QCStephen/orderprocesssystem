// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: OrderProcessSystem.proto

package proto;

public interface CancelOrderRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:OPS.CancelOrderRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * 取消的订单ID
   * </pre>
   *
   * <code>uint64 orderID = 1;</code>
   * @return The orderID.
   */
  long getOrderID();

  /**
   * <code>string time = 2;</code>
   * @return The time.
   */
  java.lang.String getTime();
  /**
   * <code>string time = 2;</code>
   * @return The bytes for time.
   */
  com.google.protobuf.ByteString
      getTimeBytes();
}
